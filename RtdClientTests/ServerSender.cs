﻿using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;

namespace RtdClientTests
{
    ////////////////////////////////////////////
    /// Sends test data to the test Rtd server
    ////////////////////////////////////////////
    public static class ServerSender
    {
        static readonly BlockingCollection<string> Queue = new BlockingCollection<string>();
        
        static ServerSender()
        {
            Thread thread1 = new Thread(SendData);
            thread1.Start();
            Thread thread2 = new Thread(Listen);
            thread2.Start();
        }

        static void SendData()
        {
            using (var sender = new RequestSocket(">tcp://localhost:5556"))
            {
                while (true)
                {
                    var item = Queue.Take();
                    sender.SendFrame(item);
                    sender.ReceiveFrameString();
                }
            }
        }

        static void Listen()
        {
            using (var listener = new ResponseSocket())
            {
                listener.Bind("tcp://*:5557");
                while (true)
                {
                    var message = listener.ReceiveFrameString();
                    listener.SendFrame(message);
                    Debug.WriteLine(message);
                    Debug.WriteLine(message);
                }
            }
        }

        public static void AddTopicData(string symbol, string topicName, dynamic topicValue)
        {
            var topicData = new TopicData() { Symbol = symbol, TopicName = topicName, TopicValue = topicValue };
            var json = JsonConvert.SerializeObject(topicData);
            Queue.Add("topic" + "|" + json);
        }

        public static void ChangeTimerInterval(int milliseconds)
        {
            Queue.Add("timer" + "|" + milliseconds.ToString());
        }

    }
}
