﻿namespace RtdClientTests
{
    public class TopicData
    {
        public string Symbol { get; set; }
        public string TopicName { get; set; }
        public dynamic TopicValue { get; set; }
    }
}
