using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Xunit;

namespace RtdClientTests
{
    public class Tests
    {
    
        [Theory]
        [InlineData("BHP", "LAST", "100")]
        [InlineData("BHP", "CHANGE", "0.1")]
        [InlineData("BHP", "MATCHPRICE", 9.90)]  // Value other than text
        [InlineData("BHP", "MATCHVOL", "9999")]
        [InlineData("BHP", "OPEN", "9.90")]
        [InlineData("BHP", "BID", "9.97")]
        [InlineData("BHP", "BIDVOL", 4000)]      // Value other than text
        [InlineData("BHP", "ASK", "9.98")]
        [InlineData("BHP", "ASKVOL", "5000")]
        [InlineData("BHP", "TRADEDVOL", "200000")]
        [InlineData("BHP", "RPT", "R")]
        [InlineData("BHP", "STATUS", "1")]
        public void SingleTopicTest(string symbol, string topicName, dynamic topicValue)
        {
            // Arrange - Add data to test rtd server
            ServerSender.AddTopicData(symbol, topicName, topicValue);

            // Act - Client subscribes to topic
            var rtdClient = new RtdClient.Client("RtdTester.Quote");
            rtdClient.AddTopic(symbol, topicName);

            // Assert - Rtd test server returns data
            var topicData = rtdClient.Rtd.DataQueue.Queue.Take();

            Assert.Equal(symbol, topicData.Symbol);
            Assert.Equal(topicName, topicData.TopicName);
            Assert.Equal(topicValue, topicData.TopicValue);
            Assert.Empty(rtdClient.Rtd.DataQueue.Queue);
        }



        //public static class DataSequenceTestData
        //{



        //    static readonly List<object[]> TestData = new List<object[]>()
        //    {
        //        new object[] { 0, "BHP",  "LAST", 98 },
        //        new object[] { 1, "BHP",  "LAST", 97.10 },
        //        new object[] { 2, "BHP",  "LAST", 96.10 },
        //        new object[] { 3, "BHP",  "LAST", 95.10 },
        //        new object[] { 4, "BHP",  "BIDVOL", 22897 },
        //        new object[] { 5, "BHP",  "ASK", 98.24 },
        //        new object[] { 6, "BHP",  "BID", 98.21 },
        //        new object[] { 7, "BHP",  "VOLUME", 9999998 },
        //        new object[] { 8, "BHP",  "VOLUME", 9999999 },
        //        new object[] { 9, "CBA",  "LAST", 44 },
        //        new object[] { 10, "CBA",  "CHANGE", 3 },
        //        new object[] { 11, "CBA",  "CHANGE", 2 },
        //        new object[] { 12, "CBA",  "BID", 44.0 },
        //        new object[] { 13, "CBA",  "BIDVOL", 18875 },
        //        new object[] { 14, "CBA",  "ASK", 43.16 },
        //        new object[] { 15, "CBA",  "ASK", 43.1 },
        //        new object[] { 16, "CBA",  "BID", 42.0 },
        //        new object[] { 17, "Z1P",  "LAST", 6.42 },
        //        new object[] { 18, "Z1P",  "LAST", 6.43 },
        //        new object[] { 19, "FMG",  "ASK", 102.45 },
        //        new object[] { 20, "REA",  "ASK", 9.99 },
        //        new object[] { 21, "A2M",  "ASK", 77.33 },
        //        new object[] { 22, "XRO",  "BID", 22.55 },
        //        new object[] { 23, "XRO",  "BID", 22.54 },
        //        new object[] { 24, "BHP",  "ASKVOL", 24999 }
        //    };

        //    static readonly List<object[]> ExpectedResults = new List<object[]>()
        //    {
        //        new object[] { "Z1P",  "LAST", 6.42 },
        //        new object[] { "FMG",  "ASK", 102.45 },
        //        new object[] { "REA",  "ASK", 9.99 },
        //        new object[] { "CBA",  "BID", 42.21 },
        //        new object[] { "A2M",  "ASK", 77.33 },
        //        new object[] { "XRO",  "BID", 22.55 },
        //        new object[] { "CBA",  "ASK", 43.16 },
        //        new object[] { "BHP",  "ASK", 98.24 },
        //        new object[] { "BHP",  "LAST", 99 },
        //        new object[] { "BHP",  "ASKVOL", 24999 },
        //        new object[] { "BHP",  "VOLUME", 9999999 },
        //        new object[] { "CBA",  "LAST", 44 },
        //        new object[] { "CBA",  "CHANGE", 2 },
        //        new object[] { "CBA",  "BIDVOL", 18875 },
        //        new object[] { "CBA",  "ASK", 43.1 },
        //        new object[] { "CBA",  "BID", 42.0 },
        //        new object[] { "XRO",  "BID", 22.54 },
        //        new object[] { "Z1P",  "LAST", 6.43 },
        //        new object[] { "FMG",  "ASK", 102.45 },
        //        new object[] { "REA",  "ASK", 9.99 },
        //        new object[] { "A2M",  "ASK", 77.33 },
        //    };
        //}



        class DataSequenceTestData
        {
            public static object[] Data()
            {
                return new object[] {
                    new object[] { 0, "BHP", "LAST", 98 },
                    new object[] { 1, "BHP", "LAST", 97.10 },
                    new object[] { 2, "BHP", "LAST", 96.10 },
                    new object[] { 3, "BHP", "LAST", 95.10 },
                    new object[] { 4, "BHP", "BIDVOL", 22897 },
                    new object[] { 5, "BHP", "ASK", 98.24 },
                    new object[] { 6, "BHP", "BID", 98.21 },
                    new object[] { 7, "BHP", "VOLUME", 9999998 },
                    new object[] { 8, "BHP", "VOLUME", 9999999 },
                    new object[] { 9, "CBA", "LAST", 44 },
                    new object[] { 10, "CBA", "CHANGE", 3 },
                    new object[] { 11, "CBA", "CHANGE", 2 },
                    new object[] { 12, "CBA", "BID", 44.0 },
                    new object[] { 13, "CBA", "BIDVOL", 18875 },
                    new object[] { 14, "CBA", "ASK", 43.16 },
                    new object[] { 15, "CBA", "ASK", 43.1 },
                    new object[] { 16, "CBA", "BID", 42.0 },
                    new object[] { 17, "Z1P", "LAST", 6.42 },
                    new object[] { 18, "Z1P", "LAST", 6.43 },
                    new object[] { 19, "FMG", "ASK", 102.45 },
                    new object[] { 20, "REA", "ASK", 9.99 },
                    new object[] { 21, "A2M", "ASK", 77.33 },
                    new object[] { 22, "XRO", "BID", 22.55 },
                    new object[] { 23, "XRO", "BID", 22.54 },
                    new object[] { 24, "BHP", "ASKVOL", 24999 }
                };
            }

            public static object[] Expected()
            {
                return new object[] {
                    new object[] { "Z1P",  "LAST", 6.42 },
                    new object[] { "FMG",  "ASK", 102.45 },
                    new object[] { "REA",  "ASK", 9.99 },
                    new object[] { "CBA", "BID", 42.21 },
                    new object[] { "A2M", "ASK", 77.33 },
                    new object[] { "XRO", "BID", 22.55 },
                    new object[] { "CBA", "ASK", 43.16 },
                    new object[] { "BHP", "ASK", 98.24 },
                    new object[] { "BHP", "LAST", 99 },
                    new object[] { "BHP", "ASKVOL", 24999 },
                    new object[] { "BHP", "VOLUME", 9999999 },
                    new object[] { "CBA", "LAST", 44 },
                    new object[] { "CBA", "CHANGE", 2 },
                    new object[] { "CBA", "BIDVOL", 18875 },
                    new object[] { "CBA", "ASK", 43.1 },
                    new object[] { "CBA", "BID", 42.0 },
                    new object[] { "XRO", "BID", 22.54 },
                    new object[] { "Z1P", "LAST", 6.43 },
                    new object[] { "FMG", "ASK", 102.45 },
                    new object[] { "REA", "ASK", 9.99 },
                    new object[] { "A2M", "ASK", 77.33 }
                };
            }

        }





    //    [Theory]
    //    [InlineData(DataSequenceTestData.Data(), DataSequenceTestData.Expected())]
    //    public void DataSequenceTest(object[] testData, object[] expected)
    //    {
    //        // Arrange - Client subscribes to topics
    //        var rtdClient = new RtdClient.Client("RtdTester.Quote");

    //        //foreach (var obj in testData)
    //        //{
    //        //    rtdClient.AddTopic((string)obj[0], "LAST");
    //        //    rtdClient.AddTopic((string)obj[0], "ASK");
    //        //    rtdClient.AddTopic((string)obj[0], "ASKVOL");
    //        //    rtdClient.AddTopic((string)obj[0], "BID");
    //        //    rtdClient.AddTopic((string)obj[0], "BIDVOL");
    //        //    rtdClient.AddTopic((string)obj[0], "CHANGE");
    //        //    rtdClient.AddTopic((string)obj[0], "VOLUME");
    //        //}

    //        //// Act Send data to Rtd server
    //        //foreach (var obj in testData)
    //        //{
    //        //   ServerSender.AddTopicData((string)obj[0], (string)obj[1], obj[2]);
    //        //}

    //        //var clientTopicDataList = new List<RtdClient.DataQueue.TopicData>();
    //        //while (true)
    //        //{
    //        //    if (rtdClient.Rtd.DataQueue.Queue.Count == 0)
    //        //    {
    //        //        break;
    //        //    }
    //        //    var taken = rtdClient.Rtd.DataQueue.Queue.Take();
    //        //    clientTopicDataList.Add(taken);
    //        //}

    //        //Assert.Equal(symbol, topicData.Symbol);

    //    }


    //    //return new List<object[]>
    //    //{
    //    //    new object[] { "Z1P",  "LAST", 6.42 },
    //    //    new object[] { "FMG",  "ASK", 102.45 },
    //    //    new object[] { "REA",  "ASK", 9.99 },
    //    //    new object[] { "CBA",  "BID", 42.21 },
    //    //    new object[] { "A2M",  "ASK", 77.33 },
    //    //    new object[] { "XRO",  "BID", 22.55 },
    //    //    new object[] { "CBA",  "ASK", 43.16 },
    //    //    new object[] { "BHP",  "ASK", 98.24 },
    //    //    new object[] { "BHP",  "LAST", 99 },
    //    //    new object[] { "BHP",  "ASKVOL", 24999 },
    //    //     new object[] { "BHP",  "VOLUME", 9999999 },
    //    //    new object[] { "CBA",  "LAST", 44 },
    //    //    new object[] { "CBA",  "CHANGE", 2 },
    //    //    new object[] { "CBA",  "BIDVOL", 18875 },
    //    //    new object[] { "CBA",  "ASK", 43.1 },
    //    //    new object[] { "CBA",  "BID", 42.0 },
    //    //    new object[] { "XRO",  "BID", 22.54 },
    //    //    new object[] { "Z1P",  "LAST", 6.43 },
    //    //    new object[] { "FMG",  "ASK", 102.45 },
    //    //    new object[] { "REA",  "ASK", 9.99 },
    //    //    new object[] { "A2M",  "ASK", 77.33 },
    //    //};





    //    //// Some data source
    //    //public void SpeedTest()
    //    //{
    //    //    // Arrange 
    //    //    ServerSender.ChangeTimerInterval(1); // 1 millisecond  
    //    //}



    }






}
