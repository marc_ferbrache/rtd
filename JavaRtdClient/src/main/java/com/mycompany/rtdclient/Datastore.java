package com.mycompany.rtdclient;
import java.util.Map;
import java.util.concurrent.*;

public class Datastore {
    
    final static Map<String, Instrument> instruments = new ConcurrentHashMap<String, Instrument>();
    
    static int addUpdateCount = 0;
    static int instrumentCount = 0;
    
    static void Add(String symbol, String quoteType, Object quoteValue)
    {
        boolean instrumentFound = instruments.containsKey(symbol);
        if (instrumentFound)
        {
                // Update existing dictionary item with the new data
                instruments.put(symbol, UpdateInstrument(quoteType, quoteValue, instruments.get(symbol)));
                addUpdateCount++;
        }
        else
        {
                // Create a new dictionary item and then update it with the new data
                Instrument instrument = new Instrument();
                instrument.setSecurity(symbol);
                instruments.put(symbol, UpdateInstrument(quoteType, quoteValue, instrument));
                instrumentCount++;
                addUpdateCount++;
        }
        
        // Send to datafeed of your choice here...
        // SendToDatafeed(instruments.get(symbol));
    }
    
    
    static Instrument UpdateInstrument(String quoteType, Object value, Instrument instrument)
        {
            switch (quoteType.toLowerCase())
            {
                case "last":
                    instrument.setLast((double)value);
                    break;
                case "change":
                    instrument.setChange((double)value);
                    break;
                case "matchprice":
                    instrument.setMatchPrice((double)value);
                    break;
                case "matchvol":
                    instrument.setMatchVol((double)value);
                    break;
                case "open":
                    instrument.setOpen((double)value);
                    break;
                case "bid":
                    instrument.setBid((double)value);
                    break;
                case "bidvol":
                    instrument.setBidVol((double) value);
                    break;
                case "ask":
                    instrument.setAsk((double)value);
                    break;
                case "askvol":
                    instrument.setAskVol((double)value);
                    break;
                case "tradedvol":
                    instrument.setTradedVol((double)value);
                    break;
                case "rpt":
                    instrument.setRpt((String)value);
                    break;
                case "status":
                    instrument.setStatus((String)value);
                    break;
            }
            return instrument;
        }
}
