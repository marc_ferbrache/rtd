package com.mycompany.rtdclient;

public class Instrument {

    private String security;
    public String getSecurity() {
        return this.security;
    }
    public void setSecurity(String str){
        this.security = str;
    }
    
    private double last;
    public double getLast() {
        return this.last;
    }
    public void setLast(double dbl){
        this.last = dbl;
    }
    
    private double change;
    public double getChange() {
        return this.change;
    }
    public void setChange(double dbl){
        this.change = dbl;
    }
    
    private double matchPrice;
    public double getMatchPrice() {
        return this.matchPrice;
    }
    public void setMatchPrice(double dbl){
        this.matchPrice = dbl;
    }
    
    private double matchVol;
    public double getMatchVol() {
        return this.matchVol;
    }
    public void setMatchVol(double dbl){
        this.matchVol = dbl;
    }
    
    private double open;
    public double getOpen() {
        return this.open;
    }
    public void setOpen(double dbl){
        this.open = dbl;
    }
    
    private double bid;
    public double getBid() {
        return this.bid;
    }
    public void setBid(double dbl){
        this.bid = dbl;
    }
    
    private double bidVol;
    public double getBidVol() {
        return this.bidVol;
    }
    public void setBidVol(double dbl){
        this.bidVol = dbl;
    }
    
    private double ask;
    public double getAsk() {
        return this.ask;
    }
    public void setAsk(double dbl){
        this.ask = dbl;
    }
    
    private double askVol;
    public double getAskVol() {
        return this.askVol;
    }
    public void setAskVol(double dbl){
        this.askVol = dbl;
    }
    
    private double tradedVol;
    public double getTradedVol() {
        return this.tradedVol;
    }
    public void setTradedVol(double dbl){
        this.tradedVol = dbl;
    }
    
    private String rpt;
    public String getRpt() {
        return this.rpt;
    }
    public void setRpt(String str){
        this.rpt = str;
    }
    
    private String status;
    public String getStatus() {
        return this.status;
    }
    public void setStatus(String str){
        this.status = str;
    }
}
