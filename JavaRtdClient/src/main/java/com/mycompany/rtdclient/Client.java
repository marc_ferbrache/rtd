package com.mycompany.rtdclient;

public class Client implements AutoCloseable{
    final Rtd rtd;        
       
    public Client()
    {
        this.rtd = new Rtd(10);
    }    
    public Client(int heartbeat)
    {
        this.rtd = new Rtd(heartbeat);  
    }
    
    public void Add(String symbol, String quoteType)
    {
        this.rtd.Add(symbol, quoteType);
    }
    
    public void close() throws Exception 
    {
        rtd.Disconnect();
    }
}
