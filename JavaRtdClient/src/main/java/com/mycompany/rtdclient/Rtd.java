package com.mycompany.rtdclient;
import com.mycompany.rtdclient.Interfaces.*;
import java.util.*;

public class Rtd implements IRTDUpdateEvent {

    final IRtdServer server;
    final Topics topics = new Topics();

    int heartbeatInterval;
    
    public int getHeartbeatInterval() {
        return this.heartbeatInterval;
    }
    public void setHeartbeatInterval(int newHeartbeatInterval){
        this.heartbeatInterval = newHeartbeatInterval;
    }
    
    public Rtd(int heartbeat)
    {
        server = IRtdServer.INSTANCE;
        this.setHeartbeatInterval(heartbeat);
        server.ServerStart(this);
    }
      
    boolean Add(String symbol, String quoteType)
    {
        // Add new topic to local topics dictionary.
        int topicId = this.topics.AddTopic(symbol, quoteType);

        // Send new topic to server, reply contains the topic value. 
        boolean boolFlag = true;
        
        // Create new topic array, to send to RTD server.
        Object[] arr = new Object[2];
        arr[0] = symbol;    // EXAMPLE: MSFT
        arr[1] = quoteType; // EXAMPLE: LASTPRICE
        
        ArrayList arrayList = new ArrayList();
        arrayList.add(arr);
        
        // Contact RTD server to register new topic (Example:  MSFT / LASTPRICE)
        Object reply = this.server.ConnectData(topicId, arrayList, boolFlag);

        // in reality this (#N/A) is going to be something different from a real server
        if (reply.toString().equals("#N/A"))
        {
            // SEND TO LOG OR NOTIFY ADMINISTRATOR
        }
        else
        {
           // Send to outgoing datastore.
           Datastore.Add(symbol, quoteType, reply);
        }
        return true;
    }
        
    
    @Override
    public void UpdateNotify() {
        // Message from server to say updates are available.

            // Get updates from server
            int topicsCount = this.topics.GetTopicCount();
            ArrayList<Object[]> refresh = (ArrayList<Object[]>)server.RefreshData(topicsCount);

            if (refresh.size() > 0)
            {
                for (int i = 0; i < refresh.size(); i++)  // Differs from .NET version
                {
                    Object[] item = new Object[2];
                    item = (Object[])refresh.get(i);
                        int topicId = (int)item[0];
   
                    Topics.Topic topic = this.topics.GetTopic(topicId);

                    // In reality this (#N/A) is going to be something different from a real server
                    if (((String)item[1]).equals("#N/A"))
                    {
                        // SEND TO LOG OR NOTIFY ADMINISTRATOR
                        continue;
                    }
                    // Send to outgoing datastore.
                    Datastore.Add(topic.getSymbol(), topic.getTopicType(), item[1]);
                }
            } 
    }

    @Override
    public void Disconnect() {
        this.server.ServerTerminate(); 
    }

}
