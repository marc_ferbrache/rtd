package com.mycompany.rtdclient;

public class Main {
    final static Client client = new Client();

    public static void main(String[] args) 
    {
        AddSymbol("BHP");
    }
       
    static void AddSymbol(String symbol)
    {
        client.Add(symbol, "OPEN");
        client.Add(symbol, "LAST");
        client.Add(symbol, "CHANGE");
        client.Add(symbol, "BID");
        client.Add(symbol, "BIDVOL");
        client.Add(symbol, "ASK");
        client.Add(symbol, "ASKVOL");
        client.Add(symbol, "VOLUME");
        client.Add(symbol, "MATCHPRICE");
        client.Add(symbol, "MATCHVOL");
        client.Add(symbol, "STATUS");
        client.Add(symbol, "RPT");
    }
}
