package com.mycompany.rtdclient;
import  java.util.HashMap;
import  java.util.Map;

public final class Topics {
    final Map<Integer, Topic> topics = new HashMap<>();
    
    int topicCount = 0;

    public int GetTopicCount()
    {
        return this.topicCount;
    }
    
    public Topic GetTopic(int id)
    {
       return this.topics.get(id);
    }
    
    public int AddTopic(String symbol, String type)    
    {
        this.topicCount++;
        Topic topic =  new Topic();
        topic.symbol = symbol;
        topic.topicType = type;
        this.topics.put(topicCount, topic);
        return this.topicCount;
        
    }
    
    public class Topic {
        private String symbol; 
        public String getSymbol() {
           return this.symbol;
        }
        public void setSymbol(String newSymbol){
           this.symbol = newSymbol;
        }
        
        private String topicType; 
        public String getTopicType() {
          return this.topicType;
        }
        public void setTopicType(String newTopicType){
           this.topicType = newTopicType;
        }
    }
}

