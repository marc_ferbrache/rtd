package com.mycompany.rtdclient.Interfaces;
import com.sun.jna.Library;

public interface IRTDUpdateEvent extends Library {
    void UpdateNotify();
    void Disconnect();
    // int HeartbeatInterval { get; set; }
}