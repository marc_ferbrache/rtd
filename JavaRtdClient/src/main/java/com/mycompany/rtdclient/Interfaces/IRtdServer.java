
package com.mycompany.rtdclient.Interfaces;
import java.util.ArrayList;
import com.sun.jna.Library;
import com.sun.jna.Native;
import java.util.UUID;  

public interface IRtdServer extends Library 
{
    //UUID uuid;
    IRtdServer INSTANCE = (IRtdServer)Native.load("rtdserver", IRtdServer.class);
    int ServerStart(IRTDUpdateEvent callback);
    Object ConnectData(int topicId, ArrayList<Object[]> str,  boolean newValues);
    ArrayList<Object[]> RefreshData(int topicCount);
    void DisconnectData(int topicId);
    int Heartbeat();
    void ServerTerminate(); 
}
