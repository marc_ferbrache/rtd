﻿namespace RtdDemoDataGenerator
{
    internal class Instrument
    {
        public string Security { get; set; }
        public double Last { get; set; }
        public double Change { get; set; }
        public double MatchPrice { get; set; }
        public double MatchVol { get; set; }
        public double Open { get; set; }
        public double Bid { get; set; }
        public double BidVol { get; set; }
        public double Ask { get; set; }
        public double AskVol { get; set; }
        public double TradedVol { get; set; }
        public string Rpt { get; set; }
        public string Status { get; set; }
    }
}
