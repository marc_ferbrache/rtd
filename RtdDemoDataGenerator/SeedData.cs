﻿namespace RtdDemoDataGenerator
{
    internal class SeedData
    {
        public string Symbol { get; set; }
        public double Last { get; set; }
        public long Cap { get; set; }
    }
}
