﻿using System.Collections.Concurrent;

namespace RtdClient
{
    public class DataQueue
    {
        public readonly BlockingCollection<TopicData> Queue = new BlockingCollection<TopicData>();

        internal void Add(string symbol, string topicName, dynamic topicValue)
        {
            this.Queue.Add(new TopicData() { Symbol = symbol, TopicName = topicName, TopicValue = topicValue });
        }

        public class TopicData
        {
            public string Symbol { get; set; }
            public string TopicName { get; set; }
            public dynamic TopicValue { get; set; }
        }
    }

}
