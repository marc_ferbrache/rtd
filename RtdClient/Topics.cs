﻿using System.Collections.Generic;

namespace RtdClient
{
    public class Topics
    {
        readonly Dictionary<int, Topic> SubscribedTopics = new Dictionary<int, Topic>();

        int topicCount = 0;

        internal int GetTopicCount()
        {
            return this.topicCount;
        }

        internal Topic GetTopic(int topicId)
        {
            return this.SubscribedTopics[topicId];
        }

        internal int AddTopic(string symbol, string topicName)
        {
            this.topicCount++;
            this.SubscribedTopics[this.topicCount] = new Topic() { Symbol = symbol, TopicName = topicName };
            return this.topicCount;
        }

        internal class Topic
        {
            public string Symbol { get; set; }
            public string TopicName { get; set; } // Example: BID, ASK, LAST etc. (anything)
        }
    }
}
