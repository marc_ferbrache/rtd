﻿using System;

namespace RtdClient
{
    public class Client 
    {
        public readonly Rtd Rtd;

        public Client(string serverName)
        {
            this.Rtd = new Rtd(serverName);
        }

        public void AddTopic(string symbol, string topic)
        {
            this.Rtd.AddTopic(symbol, topic);
        }

    }
}
