### APPLICATIONS: ###

* Real-Time Data (RTD) servers are commonly used in Excel to stream live data, particularly in financial and trading applications.
* However, using Excel for real-time data can have limitations, especially when you need to process large amounts of data or integrate with other systems.
* An RTD client can stream real-time data directly into data feeds like Kafka or other message queues, bypassing Excel or any graphical user interface (GUI). 
* This approach is particularly useful for building scalable, automated systems that require efficient data processing and distribution.

### SOURCE CODE: ###
*For those who require more control and customization, the source code is available for purchase at a price of $1,500 Australian Dollars. 
*This option is ideal for serious users who want to tailor the software to their specific needs or integrate it deeply into their existing systems.

### GET STARTED ###

1) Create a console app named RtdClientRunner.
2) Install the Nuget package RtdClient to the RtdClientRunner project. 
3) Add the code below
4) Change the Rtd Server details (from "RtdServer.Busyday") to your rtd server.
5) Modify 'Client.AddTopic(symbol, "LAST");' etc. to be whatever topics your rtd server offers.

using System.IO;

namespace RtdClientRunner
{
    class Program
    {
       static readonly RtdClient.Client Client = new RtdClient.Client("RtdServer.BusyDay");

       static void Main()
       {
          var csv = @"symbols.txt";
          var symbols = File.ReadAllLines(csv);
          foreach (var symbol in symbols)
          {
              Client.AddTopic(symbol, "OPEN");
              Client.AddTopic(symbol, "LAST");
              Client.AddTopic(symbol, "CHANGE");
              Client.AddTopic(symbol, "BID");
              Client.AddTopic(symbol, "BIDVOL");
              Client.AddTopic(symbol, "ASK");
              Client.AddTopic(symbol, "ASKVOL");
              Client.AddTopic(symbol, "TRADEDVOL");
              Client.AddTopic(symbol, "MATCHPRICE");
              Client.AddTopic(symbol, "MATCHVOL");
              Client.AddTopic(symbol, "STATUS");
              Client.AddTopic(symbol, "RPT");
          }

          while (true)
          {
              // Take() an item from the queue, blocking collection will wait until an item is available.
              var topicData = Client.Rtd.DataQueue.Queue.Take();

              // Send these in your datafeed (Kafka, RabbitMQ, etc.)
              string Symbol = topicData.Symbol;
              string topicName = topicData.TopicName;
              dynamic topicValue = topicData.TopicValue;
          }
      }
   }
}



### MORE USAGE INSTRUCTIONS: ###

* Please visit the website below for more usage examples.

Visit: https://amate.com.au