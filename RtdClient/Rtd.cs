﻿using RtdClient.Interfaces;
using System;

namespace RtdClient
{
    public class Rtd : IRTDUpdateEvent 
    {
        readonly IRtdServer Server;

        // Outgoing data queue
        public readonly DataQueue DataQueue = new DataQueue();

        // Local topics dictionary, stores topics that are to be synced with the RTD server.
        readonly Topics Topics = new Topics();

        // Used to determine if client/server are inactive during slow period (not really applicable to stock market)
        public int HeartbeatInterval { get; set; }

        public Rtd(string serverName) 
        {
            var rtdName = Type.GetTypeFromProgID(serverName);
            this.Server = (IRtdServer)Activator.CreateInstance(rtdName);
            this.HeartbeatInterval = 10;
            this.Server.ServerStart(this);
        }

        public bool AddTopic(string symbol, string topicName)
        {
            // Create new topic array, to send to the RTD server. 
            Array objects = new object[] { symbol, topicName };
            
            // Add new topic to local topics dictionary.
            var topicId = this.Topics.AddTopic(symbol, topicName);

            // Send new topic to RTD server, reply contains the topic value. 
            bool boolFlag = true;
            dynamic reply = this.Server.ConnectData(topicId, ref objects, ref boolFlag);

            if (reply != null)
            {
                // Send topic with value, to the outgoing queue.
                this.DataQueue.Add(symbol, topicName, reply);
            }
            return true;
        }

        public void UpdateNotify()
        {
            // Get updates from server
            int topicsCount = this.Topics.GetTopicCount();

            // Gets array of topics / data from RTD server
            object[,] refresh = (object[,])Server.RefreshData(ref topicsCount);

            if (refresh.Length > 0)
            {
                for (int i = 0; i < refresh.Length / 2; i++) // Older array type, divide by 2 
                {
                    var id = (int)refresh[0, i];
                    var topic = this.Topics.GetTopic(id);

                    // in reality this (#N/A) is going to be something different from a real server, probably null
                    if (Convert.ToString(refresh[1, i]).Equals("#N/A"))
                    {
                        // SEND TO LOG OR NOTIFY ADMINISTRATOR
                        continue;
                    }

                    // Send to outgoing datastore.
                    this.DataQueue.Add(topic.Symbol, topic.TopicName, refresh[1, i]);
                }
            }
        }

        public void Disconnect()
        {
            this.Server.ServerTerminate();
        }
    }
}
