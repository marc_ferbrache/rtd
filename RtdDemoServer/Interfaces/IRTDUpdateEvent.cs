﻿using System;
using System.Runtime.InteropServices;

namespace RtdTestServer.Interfaces
{
    [Guid("A43788C1-D91B-11D3-8F39-00C04F3651B8")]
    public interface IRTDUpdateEvent
    {
        void UpdateNotify();

        int HeartbeatInterval { get; set; }

        void Disconnect();
    }
}
