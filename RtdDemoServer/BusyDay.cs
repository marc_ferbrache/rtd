﻿using Newtonsoft.Json;
using RtdTestServer.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Timers;
//using Excel = Microsoft.Office.Interop.Excel; - NO EXCEL !!!!

namespace RtdDemoServer
{
    [Guid("27E72F55-CB76-40C1-9055-E99F40B6DF1A"), ProgId("RtdDemo.BusyDay"),]
    public class BusyDay : IRtdServer
    {
        readonly ConcurrentDictionary<string, SeedData> _seedData = new ConcurrentDictionary<string, SeedData>();
        readonly ConcurrentDictionary<long, Topic> _topics = new ConcurrentDictionary<long, Topic>();
        readonly ConcurrentDictionary<string, Instrument> _instruments = new ConcurrentDictionary<string, Instrument>();
        readonly Random _random = new Random();

        IRTDUpdateEvent _callback;
        Timer _timer;

        [ComVisible(true)]
        public int ServerStart(IRTDUpdateEvent callback)
        {
            _callback = callback;
            _timer = new Timer();
            _timer.Elapsed += new ElapsedEventHandler(TimerEventHandler);
            _timer.Interval = 1; 
            return 1;
        }

        public BusyDay()
        {
            foreach (var item in LoadSeedData())
            {
                _seedData.TryAdd(item.Symbol, item);
            }
        }

        private List<SeedData> LoadSeedData()
        {
            var seedData = @"C:\Users\me\Desktop\Rtd\companies_list.json";
            // Have to open file as readonly when program remotely invoked, to prevent security exception. 
            var fileStream = File.Open(seedData, FileMode.Open, FileAccess.Read);
            string json;
            using (var sr = new StreamReader(fileStream))
            {
                json = sr.ReadToEnd();
            }
            return JsonConvert.DeserializeObject<List<SeedData>>(json);
        }

        [ComVisible(true)]
        public void ServerTerminate()
        {
            if (null != _timer)
            {
                _timer.Dispose();
                _timer = null;
            }
        }

        [ComVisible(true)]
        public int Heartbeat()
        {
            return 1; // Returns 1 to excel, to let excel know the server is still alive if data is sparse.
        }

        private void TimerEventHandler(object sender, EventArgs args)
        {
            _timer.Stop();
            _callback.UpdateNotify();
        }

        [ComVisible(true)]
        public void DisconnectData(int topicId)
        {
            // Client unsubscribes a topic
            _timer.Stop();
            _topics.TryRemove(topicId, out Topic ignore);
        }

        [ComVisible(true)]
        public dynamic ConnectData(int topicId, ref Array parameters, ref bool newValues)
        {
            /////////////////////////////////////////////////////////////
            //FIRES WHEN A TOPIC ADDED - EG: BHP/ASK
            ///////////////////////////////////////////////////////////////

            var symbol = parameters.GetValue(0).ToString().ToUpper();
            var fieldType = parameters.GetValue(1).ToString().ToLower();

            var found = _instruments.TryGetValue(symbol, out Instrument instrument);
            if (!found)
            {
                // If not in instruments dict then add symbol
                _instruments.TryAdd(symbol, new Instrument() { Security = symbol });
            }

            // Add new topic, if it is already in the topics then this will be ignored 
            _topics.TryAdd(topicId, new Topic() { TopicId = topicId, Symbol = symbol, FieldType = fieldType });

            _timer.Start();

            if (!_seedData.TryGetValue(symbol, out SeedData ignore))
            {
                // Symbol not in seed data for this demo
                newValues = true;
                return null;
            }

            if (fieldType == "open")
            {
                newValues = true;
                _instruments[symbol].Open = GetClose(symbol);
                return _instruments[symbol].Open;
            }
            else if (fieldType == "last")
            {
                newValues = true;
                _instruments[symbol].Last = GetClose(symbol); // Last should start off with Open ???
                if (_instruments[symbol].Last < 0.01)
                {
                    _instruments[symbol].Last = 0;
                }    
                return _instruments[symbol].Last;
            }
            else if (fieldType == "change")
            {
                newValues = true;
                _instruments[symbol].Change = Convert.ToDouble((((instrument.Last - GetClose(symbol)) / GetClose(symbol)) * 100).ToString("0.0")); // Last Price minus Close Price) divided by Close Price x 100
                return _instruments[symbol].Change;
            }
            else if (fieldType == "bid")
            {
                newValues = true;
                _instruments[symbol].Bid = GetClose(symbol) - .01;
                return _instruments[symbol].Bid;
            }
            else if (fieldType == "bidvol")
            {
                newValues = true;
                _instruments[symbol].BidVol = GetAskBidVol();
                return _instruments[symbol].BidVol;
            }
            else if (fieldType == "ask")
            {
                newValues = true;
                _instruments[symbol].Ask = GetClose(symbol) + .01;
                return _instruments[symbol].Ask;
            }
            else if (fieldType == "askvol")
            {
                newValues = true;
                _instruments[symbol].AskVol = GetAskBidVol();
                return _instruments[symbol].AskVol;
            }
            else if (fieldType == "tradedvol")
            {
                newValues = true;
                _instruments[symbol].TradedVol = GetTradedVol(symbol);
                return _instruments[symbol].TradedVol;
            }
            else if (fieldType == "matchprice")
            {
                newValues = true;
                _instruments[symbol].MatchPrice = 0;
                return _instruments[symbol].MatchPrice;
            }
            else if (fieldType == "matchvol")
            {
                newValues = true;
                _instruments[symbol].MatchVol = 0;
                return _instruments[symbol].MatchVol;
            }
            else if (fieldType == "status")
            {
                newValues = true;
                _instruments[symbol].Status = "1"; // OPEN, PRE-OPEN, PRE-CSPA 
                return _instruments[symbol].Status;
            }
            else if (fieldType == "rpt")  // News
            {
                newValues = true;
                _instruments[symbol].Rpt = "";
                return _instruments[symbol].Rpt;
            }
            return "#N/A";
        }


        [ComVisible(true)]
        public Array RefreshData(ref int topicCount)
        {
            //////////////////////////////////////////////////////////////////////////////
            // Refreshes all topics that have changed (which is all of them in this case!)
            //////////////////////////////////////////////////////////////////////////////
            var returnTopics = new Dictionary<int, dynamic>();
            foreach (var topic in _topics)
            {
                if (!_instruments.TryGetValue(topic.Value.Symbol, out Instrument instrument))
                {
                    // Topic unavailable here

                    //JUST DONT RETURN ANYTHING (remove line below) - THAT EQUALS #N/A !!!!!!!! 
                    returnTopics.Add(topic.Value.TopicId, "#N/A"); // this will need looking at 
                    continue;
                }

                if (!_seedData.TryGetValue(topic.Value.Symbol, out SeedData ignore))
                {
                    // Topic unavailable here
                    //JUST DONT RETURN ANYTHING (remove line below) - THAT EQUALS #N/A !!!!!!!!
                    returnTopics.Add(topic.Value.TopicId, "#N/A");
                    continue;
                }

                if (topic.Value.FieldType == "close")
                {
                    var quote = GetClose(topic.Value.Symbol);
                    if (quote != instrument.Open)
                    {
                        returnTopics.Add(topic.Value.TopicId, quote);
                        _instruments[topic.Value.Symbol].Open = quote;
                    }
                }
                else if (topic.Value.FieldType == "last")
                {
                    var quote = GetLast(topic.Value.Symbol);
                    if (quote != instrument.Last)
                    {
                        if (quote > 0.00)
                        {
                            returnTopics.Add(topic.Value.TopicId, Math.Round(quote, 3));
                            _instruments[topic.Value.Symbol].Last = Math.Round(quote, 3);
                        }
                        else
                        {
                            returnTopics.Add(topic.Value.TopicId, 0);
                            _instruments[topic.Value.Symbol].Last = 0;
                            continue;
                        }
                    }
                }
                else if (topic.Value.FieldType == "change")
                {
                    var quote = Convert.ToDouble((((instrument.Last - GetClose(topic.Value.Symbol)) / GetClose(topic.Value.Symbol)) * 100).ToString("0.0"));
                    if (quote != instrument.Change)
                    {
                        returnTopics.Add(topic.Value.TopicId, quote);
                        _instruments[topic.Value.Symbol].Change = quote;
                    }
                }
                else if (topic.Value.FieldType == "bid")
                {
                    var quote = GetLast(topic.Value.Symbol) - .01;
                    if (quote != instrument.Bid)
                    {
                        returnTopics.Add(topic.Value.TopicId, Math.Round(quote, 3));
                        _instruments[topic.Value.Symbol].Bid = Math.Round(quote, 3);
                    }
                }
                else if (topic.Value.FieldType == "bidvol")
                {
                    var quote = ChangeAskBidVol(topic.Value.Symbol, true);
                    if (quote != instrument.BidVol)
                    {
                        returnTopics.Add(topic.Value.TopicId, quote);
                        _instruments[topic.Value.Symbol].BidVol = quote;
                    }
                }
                else if (topic.Value.FieldType == "ask")
                {
                    var quote = GetLast(topic.Value.Symbol) + .01;
                    if (quote != instrument.Ask)
                    {
                        returnTopics.Add(topic.Value.TopicId, Math.Round(quote, 3));
                        _instruments[topic.Value.Symbol].Ask = Math.Round(quote, 3);
                    }
                }
                else if (topic.Value.FieldType == "askvol")
                {
                    var quote = ChangeAskBidVol(topic.Value.Symbol, false);
                    if (quote != instrument.AskVol)
                    {
                        returnTopics.Add(topic.Value.TopicId, quote);
                        _instruments[topic.Value.Symbol].AskVol = quote;
                    }
                }
                else if (topic.Value.FieldType == "rpt")
                {
                    var quote = GetRpt(topic.Value.Symbol);
                    if (quote != instrument.Rpt)
                    {
                        returnTopics.Add(topic.Value.TopicId, quote);
                        _instruments[topic.Value.Symbol].Rpt = quote;
                    }
                }
            }

            var data = new object[2, returnTopics.Count];
            int i = 0;
            foreach (var item in returnTopics)
            {
                data[0, i] = item.Key;
                if (item.Value.GetType() == typeof(string))
                {
                    data[1, i] = (string)item.Value;
                }
                else if (item.Value.GetType() == typeof(double))
                {
                    data[1, i] = (double)item.Value;
                }

                i++;
            }

            topicCount = i; // topicCount passed out as a reference -  absolutely essential for it to work !!!
            _timer.Start();
            return data;
        }





        ////////////////////////////////////////////////////////////////////////////////////////////////
        /// The methods below are used to randomize the data for each company
        ////////////////////////////////////////////////////////////////////////////////////////////////
        private double GetClose(string symbol)
        {
            // Get yesterday's close from the test data
            var found = _seedData.TryGetValue(symbol, out SeedData seedData);
            if (found)
            {
                return seedData.Last;
            }
            return 0;
        }

        private double GetLast(string symbol)
        {
            var ret = _instruments[symbol].Last;
            return RandomiseDouble(ret);
        }


        private int GetAskBidVol()
        {
            return GetRandomNumber(20000, 300000);
        }

        private double ChangeAskBidVol(string symbol, bool isBid)
        {
            var instrument = _instruments[symbol];

            // Decider, if increase or decrease
            int decideIfChanged = GetRandomNumber(0, 30);

            // Dont change the value too often, to make it look realistic
            if (decideIfChanged < 25)
            {
                if (isBid)
                {
                    return instrument.BidVol;
                }
                else
                {
                    return instrument.AskVol;
                }
            }

            var changeDirection = GetRandomNumber(1, 3);
            var volumeChange = GetRandomNumber(1, 500);
            if (changeDirection == 1)
            {
                // Increase
                if (isBid)
                {
                    return instrument.BidVol + volumeChange;
                }
                else
                {
                    return instrument.AskVol + volumeChange;
                }
            }
            else
            {
                // Decrease
                if (isBid)
                {
                    return instrument.BidVol - volumeChange;
                }
                else
                {
                    return instrument.AskVol - volumeChange;
                }
            }
        }


        private string GetRpt(string symbol)
        {
            // Reporting flag - indicates if there is market news for the stock
            var instrument = _instruments[symbol];
            var rpt = GetRandomNumber(1, 10000);
            if (rpt == 999)
            {
                if (instrument.Rpt == "R")
                {
                    return "";
                }
                else
                {
                    return "R";
                }
            }
            return instrument.Rpt;
        }


        private long GetTradedVol(string symbol)
        {
            _seedData.TryGetValue(symbol, out SeedData seedData);
            RandomiseDouble(seedData.Cap);
            return Convert.ToInt64(RandomiseDouble(seedData.Cap));
        }



        private double RandomiseDouble(double orig)
        {
            int decideIfChanged = GetRandomNumber(0, 30);
            if (decideIfChanged < 25)
            {
                // Dont change the value too often
                return orig;
            }

            int rndIntPerc = GetRandomNumber(1, 9);

            var changeVal = rndIntPerc * (orig / 1000.0);

            // Decider if the price will increase or decrease
            int randPlusMinus = GetRandomNumber(1, 3);

            double retVal;
            if (randPlusMinus == 1)
            {
                // Price increase
                retVal = orig + changeVal;
            }
            else
            {
                // Price decrease
                retVal = orig - changeVal;
            }
            if (retVal < 0.0)
            {
                retVal = 0.0;
            }
            Convert.ToDouble(retVal.ToString("0.####"));
            return retVal;
        }

        int GetRandomNumber(int min, int max)
        {
            lock (_random)
            {
                return _random.Next(min, max);
            }
        }

    }

}



