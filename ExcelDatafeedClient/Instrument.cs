﻿using MessagePack;
using System;

namespace ExcelDatafeedClient
{
    [MessagePackObject]
    public class Instrument
    {
        [Key(1)]
        public string Security { get; set; }
        [Key(2)]
        public double Last { get; set; }
        [Key(3)]
        public double Change { get; set; }
        [Key(4)]
        public double MatchPrice { get; set; }
        [Key(5)]
        public double MatchVol { get; set; }
        [Key(6)]
        public double Open { get; set; }
        [Key(7)]
        public double Bid { get; set; }
        [Key(8)]
        public double BidVol { get; set; }
        [Key(9)]
        public double Ask { get; set; }
        [Key(10)]
        public double AskVol { get; set; }
        [Key(11)]
        public double TradedVol { get; set; }
        [Key(12)]
        public string Rpt { get; set; }
        [Key(13)]
        public string Status { get; set; }
        [Key(14)]
        public DateTime Time { get; set; }
        [IgnoreMember]
        public int TimeDiff { get; set; }
    }
}
