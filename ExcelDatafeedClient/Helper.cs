﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace ExcelDatafeedClient
{
    static class Helper
    {
        public static List<List<T>> Split<T>(this List<T> list, int parts)
        {
            int i = 0;
            var splits = from item in list
                         group item by i++ % parts into part
                         select part.ToList();
            return splits.ToList();
        }
    }
}
