﻿using NetMQ;
using NetMQ.Sockets;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Diagnostics;
using System.Linq;
using Excel = Microsoft.Office.Interop.Excel;
using MessagePack;
using System.Collections.Generic;
using System.IO;

namespace ExcelDatafeedClient
{
    class Program
    {
        static readonly ConcurrentDictionary<string, Instrument> Instruments = new ConcurrentDictionary<string, Instrument>();
        static Microsoft.Office.Interop.Excel.Application tApp;
        static Excel.Workbook wb = null;
        static Excel.Worksheet ws = null;
        static readonly Excel.Range range = null;

        static void Main(string[] args)
        {
            try
            {
                tApp = new Microsoft.Office.Interop.Excel.Application();
            }
            catch (Exception ex)
            {
                var test = ex.Message.ToString();
            }
            var file = @"C:\Users\me\Desktop\Rtd\ExcelEmpty.xlsm"; // put this in bin
            try
            {
                wb = tApp.Workbooks.Open(file, ReadOnly: true);
                ws = wb.Worksheets[1] as Excel.Worksheet;
            }
            catch (Exception ex)
            {
                var test = ex.Message.ToString();
            }

            tApp.Visible = true;

            Thread excelThread = new Thread(RunExcel);
            excelThread.Start();

            // Get sysmbols from text file and put them into 3 lists
            var csv = @"symbols.txt";
            var symbolsArr = File.ReadAllLines(csv);
            var symbolList = new List<string>(symbolsArr).OrderBy(x => x).ToList();
            var symbolLists = Helper.Split(symbolList, 3); // 3 lists

            // Put the processing into 3 threads, each thread having its own distinct symbols.
            int i = 1;
            foreach (var partList in symbolLists)
            {
                var tr = new Thread(() => ProcessInstruments(partList, i.ToString()));
                tr.Start();
                Thread.Sleep(500);
                i++;
            }


        }

        static void ProcessInstruments(List<string> symbols, string threadNumber)
        {
            using (var subSocket = new SubscriberSocket())
            {
                subSocket.Connect("tcp://localhost:12345");
                subSocket.Options.ReceiveHighWatermark = 0; // Turned this off. Better to just monitor traffic and act.
                foreach (var symbol in symbols)
                {
                    subSocket.Subscribe(symbol);
                }

                Int64 counter = 0;
                int perSecond = 0;
                int slowRunningCount = 0;  

                var sw = Stopwatch.StartNew();

                while (true)
                {
                    // Blocking 'ReceiveFrameBytes', slows the looping (good). 
                    var topic = subSocket.ReceiveFrameBytes();

                    var payload = subSocket.ReceiveFrameBytes();

                    Instrument instrument = MessagePackSerializer.Deserialize<Instrument>(payload);

                    instrument.TimeDiff = (DateTime.Now - instrument.Time).Milliseconds;
                   
                    if (instrument.TimeDiff > 30)
                    {
                        slowRunningCount++; 
                    }

                    Instruments[instrument.Security] = instrument;

                    perSecond++;

                    if (sw.ElapsedMilliseconds > 5000)
                    {
                        sw.Stop();
                        Console.WriteLine($"Tread: { threadNumber } - Instruments Per Seconds: { (perSecond / 5).ToString() } - Slow Running Count: { slowRunningCount }");

                        // This can be use to trigger alarms.  
                        slowRunningCount = 0;

                        perSecond = 0;
                        sw.Restart();
                    }

                    counter++;
                }
            }
        }



        static void RunExcel()
        {
            Thread.Sleep(9000);
            while (true)
            {
                object[,] arr = new object[Instruments.Count, 14];
                var rowCount = 0;
                foreach (var row in Instruments.OrderBy(x => x.Key))
                {
                    var instrument = (Instrument)(row.Value);
                    arr[rowCount, 0] = instrument.Security;
                    arr[rowCount, 1] = instrument.Last;
                    arr[rowCount, 2] = instrument.Change;
                    arr[rowCount, 3] = instrument.MatchPrice;
                    arr[rowCount, 4] = instrument.MatchVol;
                    arr[rowCount, 5] = instrument.Bid;
                    arr[rowCount, 6] = instrument.BidVol;
                    arr[rowCount, 7] = instrument.Ask;
                    arr[rowCount, 8] = instrument.AskVol;
                    arr[rowCount, 9] = instrument.TradedVol;
                    arr[rowCount, 10] = instrument.Open;
                    arr[rowCount, 11] = instrument.Rpt;
                    arr[rowCount, 12] = instrument.Status;
                    arr[rowCount, 13] = instrument.TimeDiff;
                    rowCount++;
                }
                Excel.Range c1 = (Excel.Range)ws.Cells[2, 1];
                Excel.Range c2 = (Excel.Range)ws.Cells[Instruments.Count + 1, 14];
                Excel.Range range = ws.get_Range(c1, c2);
                range.Value = arr;
            }
        }
    }
}