﻿using MessagePack;
using System;

namespace RtdClientRunner
{
    [MessagePackObject]
    public class Instrument
    {
        [Key(1)]
        public string Security { get; set; }
        [Key(2)]
        public double Last { get; set; }
        [Key(3)]
        public double Change { get; set; }
        [Key(4)]
        public double MatchPrice { get; set; }
        [Key(5)]
        public double MatchVol { get; set; }
        [Key(6)]
        public double Open { get; set; }
        [Key(7)]
        public double Bid { get; set; }
        [Key(8)]
        public double BidVol { get; set; }
        [Key(9)]
        public double Ask { get; set; }
        [Key(10)]
        public double AskVol { get; set; }
        [Key(11)]
        public double TradedVol { get; set; }
        [Key(12)]
        public string Rpt { get; set; }
        [Key(13)]
        public string Status { get; set; }
        [Key(14)]
        public DateTime Time { get; set; }


        internal static Instrument UpdateInstrumentTopic(Instrument instrument, string topicName, dynamic topicValue)
        {
            switch (topicName.ToLower())
            {
                case "last":
                    instrument.Last = Convert.ToDouble(topicValue);
                    break;
                case "change":
                    instrument.Change = Convert.ToDouble(topicValue);
                    break;
                case "matchprice":
                    instrument.MatchPrice = Convert.ToDouble(topicValue);
                    break;
                case "matchvol":
                    instrument.MatchVol = Convert.ToDouble(topicValue);
                    break;
                case "open":
                    instrument.Open = Convert.ToDouble(topicValue);
                    break;
                case "bid":
                    instrument.Bid = Convert.ToDouble(topicValue);
                    break;
                case "bidvol":
                    instrument.BidVol = Convert.ToDouble(topicValue);
                    break;
                case "ask":
                    instrument.Ask = Convert.ToDouble(topicValue);
                    break;
                case "askvol":
                    instrument.AskVol = Convert.ToDouble(topicValue);
                    break;
                case "tradedvol":
                    instrument.TradedVol = Convert.ToDouble(topicValue);
                    break;
                case "rpt":
                    instrument.Rpt = (string)topicValue;
                    break;
                case "status":
                    instrument.Status = (string)topicValue;
                    break;
            }
            instrument.Time = DateTime.Now;
            return instrument;
        }

    }
}
