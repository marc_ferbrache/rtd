﻿using MessagePack;
using NetMQ;
using NetMQ.Sockets;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace RtdClientRunner
{
    class Program
    {
        static readonly ConcurrentDictionary<string, Instrument> Instruments = new ConcurrentDictionary<string, Instrument>();

        static readonly RtdClient.Client Client = new RtdClient.Client("rtddemo.busyday");

        static void Main()
        {
            var csv = @"symbols.txt";
            var symbols = File.ReadAllLines(csv);
            foreach (var symbol in symbols)  
            {
                Client.AddTopic(symbol, "OPEN");
                Client.AddTopic(symbol, "LAST");
                Client.AddTopic(symbol, "CHANGE");
                Client.AddTopic(symbol, "BID");
                Client.AddTopic(symbol, "BIDVOL");
                Client.AddTopic(symbol, "ASK");
                Client.AddTopic(symbol, "ASKVOL");
                Client.AddTopic(symbol, "TRADEDVOL");
                Client.AddTopic(symbol, "MATCHPRICE");
                Client.AddTopic(symbol, "MATCHVOL");
                Client.AddTopic(symbol, "STATUS");
                Client.AddTopic(symbol, "RPT");
            }

            using (var pubSocket = new PublisherSocket())
            {
                Console.WriteLine("Publisher socket binding...");
                pubSocket.Options.SendHighWatermark = 0;
                pubSocket.Bind("tcp://*:12345");
                Thread.Sleep(3000);

                var sw = new Stopwatch();
                sw.Start();

                int counter = 0;

                while (true)
                {

                    // Take() an item from the queue, blocking collection will wait until an item is available.
                    var topicData = Client.Rtd.DataQueue.Queue.Take();
                    
                    // Update an instrument with the new topic value (Example: BID = 22.43) 
                    var instrument = UpdateInstrument(topicData.Symbol, topicData.TopicName, topicData.TopicValue);

                    byte[] data = MessagePackSerializer.Serialize(instrument);

                    pubSocket.SendMoreFrame($"{ instrument.Security }").SendFrame(data);

                    counter++;

                    if (sw.ElapsedMilliseconds > 5000)
                    {
                        var itemsInQueue = Client.Rtd.DataQueue.Queue.Count; // Calling 'Count' on BlockingCollection is costly. 
                        Console.WriteLine("Queued Topic Values: " + itemsInQueue.ToString());
                        Console.WriteLine("Instruments Sent Per Second: " + (counter / 5).ToString());
                        counter = 0;
                        sw.Restart();
                    }

                    
                }
            }
        }

        static Instrument UpdateInstrument(string symbol, string topicName, dynamic topicValue)
        {
            var instrumentFound = Instruments.TryGetValue(symbol, out Instrument instrument);
            if (instrumentFound)
            {
                // Update existing dictionary item with the new data
                Instruments[symbol] = Instrument.UpdateInstrumentTopic(instrument, topicName, topicValue);
            }
            else
            {
                // Create a new dictionary item / symbol, then update it with the new data
                instrument = new Instrument() { Security = symbol };
                instrument = Instrument.UpdateInstrumentTopic(instrument, topicName, topicValue);
                if (!Instruments.TryAdd(symbol, instrument))
                {
                    throw new Exception($"FATAL ERROR !!  COULD NOT ADD SYMBOL - { symbol } ");
                }
            }
            return Instruments[symbol];
        }

    }
}
