﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ExcelBuilder
{
    class Program
    {
        // Softcode - RtdDemo.BusyDay

        static void Main()
        {
            using (var workbook = new XLWorkbook())
            {
                var file = @"C:\Users\me\Desktop\Rtd\companies_list.json"; // put in bin

                // soft code RtdDemo.BusyDay

                var json = File.ReadAllText(file);
                var data =  JsonConvert.DeserializeObject<List<SymbolData>>(json);
                data = data.OrderBy(x => x.Symbol).ToList();
                var worksheet = workbook.Worksheets.Add("RTD");

                worksheet.Cell("A1").Value = "Security";
                worksheet.Cell("A1").Style.Font.Bold = true;
                worksheet.Cell("B1").Value = "Last";
                worksheet.Cell("B1").Style.Font.Bold = true;
                worksheet.Cell("C1").Value = "Change %";
                worksheet.Cell("C1").Style.Font.Bold = true;
                worksheet.Cell("D1").Value = "Match Price";
                worksheet.Cell("D1").Style.Font.Bold = true;
                worksheet.Cell("E1").Value = "Match Vol";
                worksheet.Cell("E1").Style.Font.Bold = true;
                worksheet.Cell("F1").Value = "Bid";
                worksheet.Cell("F1").Style.Font.Bold = true;
                worksheet.Cell("G1").Value = "Bid Vol";
                worksheet.Cell("G1").Style.Font.Bold = true;
                worksheet.Cell("H1").Value = "Ask";
                worksheet.Cell("H1").Style.Font.Bold = true;
                worksheet.Cell("I1").Value = "Ask Vol";
                worksheet.Cell("I1").Style.Font.Bold = true;
                worksheet.Cell("J1").Value = "Volume";
                worksheet.Cell("J1").Style.Font.Bold = true;
                worksheet.Cell("K1").Value = "Open";
                worksheet.Cell("K1").Style.Font.Bold = true;
                worksheet.Cell("L1").Value = "Report Flag";
                worksheet.Cell("L1").Style.Font.Bold = true;
                worksheet.Cell("M1").Value = "Status";
                worksheet.Cell("M1").Style.Font.Bold = true;

                int i = 2; // skip header row
                foreach(var item in data)
                {
                    worksheet.Cell("A" + i + "").Value = "" + item.Symbol + "";
                    worksheet.Cell("A" + i + "").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    worksheet.Cell("B" + i + "").FormulaA1 = "=RTD(\"RtdDemo.BusyDay\",,A" + i.ToString() + ",\"last\")";
                    worksheet.Cell("C" + i + "").FormulaA1 = "=RTD(\"RtdDemo.BusyDay\",,A" + i.ToString() + ",\"change\")";
                    worksheet.Cell("D" + i + "").FormulaA1 = "=RTD(\"RtdDemo.BusyDay\",,A" + i.ToString() + ",\"matchprice\")";
                    worksheet.Cell("E" + i + "").FormulaA1 = "=RTD(\"RtdDemo.BusyDay\",,A" + i.ToString() + ",\"matchvol\")";
                    worksheet.Cell("F" + i + "").FormulaA1 = "=RTD(\"RtdDemo.BusyDay\",,A" + i.ToString() + ",\"bid\")";
                    worksheet.Cell("G" + i + "").FormulaA1 = "=RTD(\"RtdDemo.BusyDay\",,A" + i.ToString() + ",\"bidvol\")";
                    worksheet.Cell("H" + i + "").FormulaA1 = "=RTD(\"RtdDemo.BusyDay\",,A" + i.ToString() + ",\"ask\")";
                    worksheet.Cell("I" + i + "").FormulaA1 = "=RTD(\"RtdDemo.BusyDay\",,A" + i.ToString() + ",\"askvol\")";
                    worksheet.Cell("J" + i + "").FormulaA1 = "=RTD(\"RtdDemo.BusyDay\",,A" + i.ToString() + ",\"tradedvol\")";
                    worksheet.Cell("K" + i + "").FormulaA1 = "=RTD(\"RtdDemo.BusyDay\",,A" + i.ToString() + ",\"open\")";
                    worksheet.Cell("L" + i + "").FormulaA1 = "=RTD(\"RtdDemo.BusyDay\",,A" + i.ToString() + ",\"rpt\")";
                    worksheet.Cell("M" + i + "").FormulaA1 = "=RTD(\"RtdDemo.BusyDay\",,A" + i.ToString() + ",\"status\")";
                    i++;
                }
                workbook.SaveAs(@"C:\Users\me\Desktop\Rtd\Feed.xlsm");
            }
        }
    }

    public class SymbolData
    {
        public string Symbol { get; set; }
        public double Last { get; set; }
        public long Cap { get; set; }
    }
}
