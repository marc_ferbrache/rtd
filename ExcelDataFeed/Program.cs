﻿using Excel = Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Linq;
using System.IO;

namespace ExcelDataFeed
{
    class Program
    {
        [DllImport("Kernel32")] // Used to trap closing console app, so can perform Excel cleanup
        private static extern bool SetConsoleCtrlHandler(SetConsoleCtrlEventHandler handler, bool add);

        private delegate bool SetConsoleCtrlEventHandler(CtrlType sig); // Used to trap closing console app

        private enum CtrlType  // Used to trap closing console app
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }


        readonly static Dictionary<string, Instrument> instruments = new Dictionary<string, Instrument>();
        static Microsoft.Office.Interop.Excel.Application tApp;
        static Excel.Workbook wb = null;
        static Excel.Worksheet ws = null;
        static Excel.Range range = null;



        static void Main(string[] args) 
        {
            SetConsoleCtrlHandler(Handler, true); // Used to trap closing console app

            while (true)
            {
                try
                {
                    tApp = new Microsoft.Office.Interop.Excel.Application();
                    break;
                }
                catch { }
            }
            try
            {
                var file = @"C:\Users\me\Desktop\Rtd\Feed.xlsm";
                while (true)
                {
                    try
                    {
                        wb = tApp.Workbooks.Open(file, ReadOnly: true);
                        break;
                    }
                    catch { }
                }
                tApp.Visible = true;
                

                Console.WriteLine("Opening Excel...");
                Thread.Sleep(4000); // Let it calm down
                Console.WriteLine("Launching Scan....");
                Thread.Sleep(2000);

                int rows;
                while (true)
                {
                    try
                    {
                        ws = wb.Worksheets[1] as Excel.Worksheet;
                        rows = ws.UsedRange.Rows.Count;
                        range = ws.Range["A2", "M" + rows.ToString()];
                        break;
                    }
                    catch(Exception ex) 
                    {
                        var test1 = ex;
                    }
                }


                var swIps = new Stopwatch();
                swIps.Start();
                var addUpdates = 0;
                var loopCount = 0;

                while (true)
                {
                    try
                    {
                        var array = range.Value;
                        var s = new Stopwatch();
                        s.Start();
                        for (int i = 1; i <= rows -1; i++)
                        {
                            var instrument = new Instrument();
                            try
                            {
                                instrument.Security = Convert.ToString(array[i, 1]);
                                instrument.Last = array[i, 2];
                                instrument.Change = array[i, 3];
                                instrument.MatchPrice = array[i, 4];
                                instrument.MatchVol = array[i, 5];
                                instrument.Bid = array[i, 6];
                                instrument.BidVol = array[i, 7];
                                instrument.Ask = array[i, 8];
                                instrument.AskVol = array[i, 9];
                                //#####################################################################################################################
                                //instrument.TradedVol = array[i, 10]; why remmed?
                                instrument.Open = array[i, 11];
                                instrument.Rpt = Convert.ToString(array[i, 12]);
                                instrument.Status = Convert.ToString(array[i, 13]);
                            }
                            catch (Exception ex)
                            {
                                var e = ex.Message.ToString();
                            }

                            if (instruments.TryGetValue(instrument.Security,  out Instrument result))
                            {
                                // Compare new instrument to existiong instrument here
                                if (ToJson(instrument) != ToJson(result))
                                {
                                    // Update existing instrument
                                    instruments[instrument.Security] = result;
                                    addUpdates++;
                                }
                                else
                                {
                                    // Do nothing - as the instrument was unchanged
                                }
                            }
                            else
                            {
                                // Add new instrument
                                instruments.Add(instrument.Security, instrument);
                                addUpdates++;
                            }
                        }
                        s.Stop();
                        var test = s.ElapsedMilliseconds;
                        loopCount++;
                        if (swIps.ElapsedMilliseconds > 1000)
                        {
                            swIps.Stop();

                            Console.WriteLine("Instruments Per Second : " + (addUpdates).ToString());
                            Console.WriteLine("Loopcount Per  Second : " + (loopCount).ToString());
                            loopCount = 0;
                            addUpdates = 0;
                            swIps.Restart();
                        }
            
                    }
                    catch (Exception ex)
                    {
                        // Sometimes excel is an orphaned process, or something to do with it is fucked
                        if (ex.HResult == -2147417846)
                        {
                            Console.Beep();
                        }
                        else
                        {
                            var test = ex;
                        }
                    } 
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message.ToString();
            }
            finally
            {

            }
        }

        static string ToJson(Instrument instrument)
        {
            // Much faster than JsonConvert.SerializeObject (x10), does not use reflection
            StringWriter sw = new StringWriter();
            JsonTextWriter writer = new JsonTextWriter(sw);
            writer.WriteStartObject();
            writer.WritePropertyName("security");
            writer.WriteValue(instrument.Security);
            writer.WritePropertyName("last");
            writer.WriteValue(instrument.Last);
            writer.WritePropertyName("change");
            writer.WriteValue(instrument.Change);
            writer.WritePropertyName("matchPrice");
            writer.WriteValue(instrument.MatchPrice);
            writer.WritePropertyName("matchVol");
            writer.WriteValue(instrument.MatchVol);
            writer.WritePropertyName("open");
            writer.WriteValue(instrument.Open);
            writer.WritePropertyName("bid");
            writer.WriteValue(instrument.Bid);
            writer.WritePropertyName("bidVol");
            writer.WriteValue(instrument.BidVol);
            writer.WritePropertyName("ask");
            writer.WriteValue(instrument.Ask);
            writer.WritePropertyName("askVol");
            writer.WriteValue(instrument.AskVol);
            writer.WritePropertyName("tradedVol");
            writer.WriteValue(instrument.TradedVol);
            writer.WritePropertyName("rpt");
            writer.WriteValue(instrument.Rpt);
            writer.WritePropertyName("status");
            writer.WriteValue(instrument.Status);
            writer.WriteEndObject();
            return sw.ToString();
        }

        private static bool Handler(CtrlType signal)
        {
            switch (signal)
            {
                case CtrlType.CTRL_BREAK_EVENT:
                case CtrlType.CTRL_C_EVENT:
                case CtrlType.CTRL_LOGOFF_EVENT:
                case CtrlType.CTRL_SHUTDOWN_EVENT:
                case CtrlType.CTRL_CLOSE_EVENT:
                    try
                    {
                        wb.Close(0);
                    }
                    catch { }
                    try
                    {
                        tApp.Quit();
                    }
                    catch { }
                    if (range != null) Marshal.ReleaseComObject(range);
                    if (ws != null) Marshal.ReleaseComObject(ws);
                    if (wb != null) Marshal.ReleaseComObject(wb);
                    if (tApp != null) Marshal.ReleaseComObject(tApp);

                    Environment.Exit(0);
                    return false;
                default:
                    return false;
            }
        }



    }
}
