﻿using NetMQ.Sockets;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RtdTestServer
{
    class Datastore
    {
        // key (topic) = bhp_bid, fgm_last etc. Dynamic = topic value  
        public static ConcurrentDictionary<string, dynamic> Data = new ConcurrentDictionary<string, dynamic>();

        // REDIS 
        // ***********
        // 1) This will be where topics will be obtained from redis.
        // 2) The ConcurrentDictionary above will be eliminAted in favour of redis. 
        // 3) Using Redis will allow the Rtd server to be used by 2 different clients simultaneously. 
        // 4) The Unit tests will be altered to also populate Redis.
        // 5) A data generator program (using the seed data) will generate data.
        // 6) Another program willl also allow the replay of stock marget data (to Redis). 
        // 7) Stored in Redis as Topics (not instrument).
        // 8) Stored in generator program as instruments.

        // A method can be created...  dynamic GetTopicData(string symbol, string topicName) 
        // Very interesting to see the speed difference of looping redis

        // RtdClient
        // RtdClientRunner
        // RtdClientIntegrationTests
        // RtdTestServer
        // RtdVisualTestGenerator
        // RtdVisualTestClient

        // ###################################################################################################
        // BATCH INCOMMING TOPICS INTO GROUPS OF 100 (OR 10 MILLISECONDS), THIS WILL REDUCE NETWORK ACTIVITY 
        // MASSIVELY. CREATE DOUBLE PIPING ROUTINE (SPLIT WITHIN A SPLIT). DONT USE SERIALIZER / DESERIALIZER
        // ###################################################################################################


    }
}
