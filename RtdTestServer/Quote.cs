﻿using RtdTestServer.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Timers;

namespace RtdTestServer
{
    [Guid("9E82208B-236E-4AFA-9D60-908F1A19C32C"), ProgId("RtdTester.Quote"),]
    public class Quote : IRtdServer
    {
        // Record of topics subscribed to. 
        readonly ConcurrentDictionary<long, Topic> Topics = new ConcurrentDictionary<long, Topic>();

        // Listens for incoming unit test data and pushes it to datastore.
        readonly TestingService TestingService = new TestingService();

        IRTDUpdateEvent _callback;
        public static Timer _timer;

        [ComVisible(true)]
        public int ServerStart(IRTDUpdateEvent callback)
        {
            _callback = callback;
            _timer = new Timer();
            _timer.Elapsed += new ElapsedEventHandler(TimerEventHandler);
            _timer.Interval = 30; // milliseconds (100 is a bit like looping over excel - on my machine !)
            return 1;
        }

        [ComVisible(true)]
        public void ServerTerminate()
        {
            if (null != _timer)
            {
                _timer.Dispose();
                _timer = null;
            }
        }

        [ComVisible(true)]
        public int Heartbeat()
        {
            return 1;
        }

        [ComVisible(true)]
        public dynamic ConnectData(int topicId, ref Array parameters, ref bool newValue)
        {
            dynamic retVal;

            var symbol = parameters.GetValue(0).ToString().ToUpper();
            var topicName = parameters.GetValue(1).ToString().ToLower();

            _timer.Start();
           
            // Add new topic, if it is already in the topics then this will be ignored 
            Topics.TryAdd(topicId, new Topic() { TopicId = topicId, Symbol = symbol, TopicName = topicName });

            // Create Dictionary Key 
            var key = symbol.ToLower() + "_" + topicName.ToLower();

            if (Datastore.Data.TryGetValue(key, out dynamic topicData))
            {
                newValue = true;     // Notify excel of new value
                retVal = topicData;  // Return value
            }
            else
            {
                newValue = true;   // This Rtd server is used by multiple clients (Excel & RtdClientd). So always set true !   
                retVal = null;
            }

            return retVal;
        }


        [ComVisible(true)]
        public void DisconnectData(int topicId)
        {
            // Client unsubscribes a topic, implemented but probably never used.
            _timer.Stop();
            Topics.TryRemove(topicId, out Topic ignore);
        }


        private void TimerEventHandler(object sender, EventArgs args)
        {
            _timer.Stop();
            _callback.UpdateNotify();
        }


        [ComVisible(true)]
        public Array RefreshData(ref int topicCount)
        {
            var returnTopics = new Dictionary<int, dynamic>();
            foreach (var topic in Topics)
            {
                var key = topic.Value.Symbol.ToLower() + "_" + topic.Value.TopicName.ToLower();
                if (!Datastore.Data.TryGetValue(key, out dynamic topicData))
                {
                    // Topic unavailable here
                    returnTopics.Add(topic.Value.TopicId, "#N/A"); // this will need looking at, as it is highly unlikely to be correct 
                    continue;
                }
                else
                {
                    returnTopics.Add(topic.Value.TopicId, topicData);
                }
            }
            int i = 0;
            var data = new object[2, returnTopics.Count];
            foreach (var item in returnTopics)
            {
                data[0, i] = item.Key;
                data[1, i] = item.Value;
                i++;
            }
            topicCount = i; // topicCount passed out as a reference -  absolutely essential for it to work !!!
            _timer.Start();
            return data;
        }

    }
}
