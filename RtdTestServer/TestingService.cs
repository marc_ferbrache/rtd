﻿using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Threading;

namespace RtdTestServer
{
    public class TestingService
    {
        public static BlockingCollection<string> Logs = new BlockingCollection<string>();

        public TestingService()
        {
            Thread thread1 = new Thread(ListenForData);
            thread1.Start();
            Thread thread2 = new Thread(TransmitLogMessages);
            thread2.Start();
        }

        /// <summary>
        /// Listens for incomming data from unit tests project
        /// </summary>
        void ListenForData()
        {
            using (var listener = new ResponseSocket("@tcp://127.0.0.1:5556"))
            {
                var testData = new TestData();
                while (true)
                {
                    string message = listener.ReceiveFrameString();
                    listener.SendFrame(message);
                    var arr = message.Split('|');
                    var command = arr[0];
                    var data = arr[1];

                    if (command.ToLower().Equals("topic"))
                    {
                        // Adds topic data values to datastore
                        testData = JsonConvert.DeserializeObject<TestData>(data);
                        var key = testData.Symbol.ToLower() + "_" + testData.TopicName.ToLower();
                        Datastore.Data[key] = testData.TopicValue;
                    }
                    if (command.ToLower().Equals("timer"))
                    {
                        Quote._timer.Stop();
                        Quote._timer.Interval = Convert.ToInt32(data);
                        Quote._timer.Start();
                    }
                }
            }
        }

        /// <summary>
        /// Used for debugging problems, sends log message back to unit test project
        /// </summary>
        void TransmitLogMessages()
        {
            using (var sender = new RequestSocket(">tcp://127.0.0.1:5557"))
            {
                var testData = new TestData();
                while (true)
                {
                    var message = Logs.Take();
                    sender.SendFrame(message);
                    sender.ReceiveFrameString();
                }
            }
        }


        public class TestData
        {
            public string Symbol { get; set; }
            public string TopicName { get; set; }
            public dynamic TopicValue { get; set; }
        }

    }
}
