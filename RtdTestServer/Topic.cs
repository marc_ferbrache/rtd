﻿namespace RtdTestServer
{
    internal class Topic
    {
        public int TopicId { get; set; }
        public string Symbol { get; set; }
        public string TopicName { get; set; }
    }
}
